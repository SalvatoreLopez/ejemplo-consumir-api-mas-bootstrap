const contenedor = document.getElementById('contenedor');
const url = "http://localhost/api-netflix/public/api/v1/peliculas";

const fetchApi = async (url) => {
    var response = await fetch(url);
    return response.json();
}

fetchApi(url)
    .then((peliculas) => {
        peliculas.forEach(pelicula => {
            contenedor.innerHTML += ` 
            <div class="col-6">
                <video class="w-100" src="${pelicula.url_video}" controls></video>
                <div class="media">
                    <img class="img-fluid" src="${pelicula.url_cover}" alt="Generic placeholder image">
                    <div class="media-body">
                        <h1 class="h6"> ${pelicula.titulo}</h1>
                        ${pelicula.descripcion}
                    </div>
                </div>
            </div>`;
        });
    })