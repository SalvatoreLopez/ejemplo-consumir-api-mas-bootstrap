var contenedor = document.getElementById("contenedor");
fetch("https://randomuser.me/api/?results=100",{
    method : 'GET'
})
.then((respuesta)=>respuesta.json())
.then((informacion)=>{
    var usuarios = informacion.results; 
    usuarios.forEach((usuario)=>{
        contenedor.innerHTML += `<div class="col-6">
                                        <div class="card mt-2 p-2">
                                            <div class="media">
                                            <img class="mr-3 border" src="${ usuario.picture.large }">
                                            <div class="media-body">
                                                <p>Nombre: ${ usuario.name.first }</p>
                                                <p>Genero: ${ usuario.gender }</p>
                                                <p>Numero celular: ${ usuario.cell }</p>
                                                <p>Telefono: ${ usuario.phone }</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>`;
    });
})
.catch((error)=>console.log(error));