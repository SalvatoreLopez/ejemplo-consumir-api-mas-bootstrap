const listaDeFrutas = document.querySelector("#listaDeFrutas");
const botonBuscar = document.querySelector("#botonBuscar");
const inputBuscar = document.querySelector("#buscar");

const frutas = [
    {
        "nombre": "Platano",
        "precio": 15.90
    },
    {
        "nombre": "Mango",
        "precio": 13.00
    },
    {
        "nombre": "Uva",
        "precio":  45.00
    },
    {
        "nombre": "Papaya",
        "precio":   67.00
    }
];
const filtrar  = () => {
    listaDeFrutas.innerHTML = '';
    var termino  = inputBuscar.value.toLowerCase();
    frutas.forEach( ( fruta ) => {
        let nomnbreFruta = fruta.nombre.toLowerCase();
        if( nomnbreFruta.indexOf( termino ) !== -1 )
            listaDeFrutas.innerHTML += `<li>Fruta: ${ fruta.nombre } Precio: ${ fruta.precio }</li>`;
    });

    if( listaDeFrutas.innerHTML === '' )
        listaDeFrutas.innerHTML = "no se encontro producto";
}

inputBuscar.addEventListener('keyup',filtrar);
botonBuscar.addEventListener('click',filtrar);

filtrar();